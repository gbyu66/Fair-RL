3
��^�Y  �               @   s�   d dl Z d dlmZ d dlZd dlZd dlZd dlm	Z	 d dl
mZmZmZmZmZ d dlmZmZ d dlmZ d dlmZmZmZmZmZ d dlmZ G d	d
� d
e�ZG dd� de�ZdS )�    N)�deque)�logger)�explained_variance�tf_util�ActorCriticRLModel�SetVerbosity�TensorboardWriter)�ActorCriticPolicy�RecurrentActorCriticPolicy)�AbstractEnvRunner)�discount_with_dones�	Scheduler�mse�Deri_GGI�total_episode_reward_logger)�	safe_meanc                   sP   e Zd ZdZd� fdd�	Zdd� Zdd� Zddd�Zddd�Zddd�Z	�  Z
S )�A2Ca�  
    The A2C (Advantage Actor Critic) model class, https://arxiv.org/abs/1602.01783

    :param policy: (ActorCriticPolicy or str) The policy model to use (MlpPolicy, CnnPolicy, CnnLstmPolicy, ...)
    :param env: (Gym environment or str) The environment to learn from (if registered in Gym, can be str)
    :param gamma: (float) Discount factor
    :param n_steps: (int) The number of steps to run for each environment per update
        (i.e. batch size is n_steps * n_env where n_env is number of environment copies running in parallel)
    :param vf_coef: (float) Value function coefficient for the loss calculation
    :param ent_coef: (float) Entropy coefficient for the loss calculation
    :param max_grad_norm: (float) The maximum value for the gradient clipping
    :param learning_rate: (float) The learning rate
    :param alpha: (float)  RMSProp decay parameter (default: 0.99)
    :param epsilon: (float) RMSProp epsilon (stabilizes square root computation in denominator of RMSProp update)
        (default: 1e-5)
    :param lr_schedule: (str) The type of scheduler for the learning rate update ('linear', 'constant',
                              'double_linear_con', 'middle_drop' or 'double_middle_drop')
    :param verbose: (int) the verbosity level: 0 none, 1 training information, 2 tensorflow debug
    :param tensorboard_log: (str) the log location for tensorboard (if None, no logging)
    :param _init_setup_model: (bool) Whether or not to build the network at the creation of the instance
                              (used only for loading)
    :param policy_kwargs: (dict) additional arguments to be passed to the policy on creation
    :param full_tensorboard_log: (bool) enable additional logging when using tensorboard
        WARNING: this logging can take a lot of space quickly
    :param seed: (int) Seed for the pseudo-random generators (python, numpy, tensorflow).
        If None (default), use random seed. Note that if you want completely deterministic
        results, you must set `n_cpu_tf_sess` to 1.
    :param n_cpu_tf_sess: (int) The number of threads for TensorFlow operations
        If None, the number of cpu of the current machine will be used.
    �G�z��?�   �      �?�{�G�z�?�      �?�Ǻ���F?��h㈵��>�constantr   NTFc          
      s
  t t| �j|||d||||d� || _|| _|| _|| _|	| _|| _|| _	|| _
|
| _|| _|| _|| _|| _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _d | _ d | _!d | _"d | _#d | _$d | _%d | _&|�r| j'�  d S )NT)�policy�env�verbose�requires_vec_env�_init_setup_model�policy_kwargs�seed�n_cpu_tf_sess)(�superr   �__init__�n_steps�gamma�vf_coef�ent_coef�max_grad_norm�alpha�epsilon�lr_schedule�learning_rate�tensorboard_log�full_tensorboard_log�reward_space�weight_coef�graph�sess�learning_rate_ph�n_batch�
actions_ph�advs_ph�
rewards_ph�pg_loss�vf_loss�entropy�params�apply_backprop�train_model�
step_model�step�
proba_step�value�initial_state�learning_rate_schedule�summary�episode_reward�ini_obs_value�reward_n�setup_model)�selfr   r   r0   r1   r&   r%   r'   r(   r)   r-   r*   r+   r,   r   r.   r   r    r/   r!   r"   )�	__class__� �D/home/nfs/msiddique/stable-baselines/stable_baselines/a2c_ggi/a2c.pyr$   0   sR    
zA2C.__init__c             C   s6   | j }t| jtjj�r&|j| j|jfS |j| j|j	fS )N)
r>   �
isinstance�action_space�gym�spaces�Discrete�obs_phr6   r   �deterministic_action)rJ   r   rL   rL   rM   �_get_pretrain_placeholdersc   s    zA2C._get_pretrain_placeholdersc          (   C   s   t | j���� tj� | _| jj� ��� | j| j� tj	| j
| jd�| _| j| j | _| j| _d }d }t| jt�r�| j}| j| j }| j| j| j| j| j| jd|fddi| j��}tjddtjd�d��6 | j| j| j| j| j| j| j|fddi| j��}W d Q R X tjddd	���� |jjd gd
d�| _tjtjd | jgdd�| _tjtjd | jgdd�| _tjtjg dd�| _ tjtj| jgdd�| _!tjtj| jgdd�| _"|j#j$| j�}tj%|j#j&� �| _&| jtj'|dd� }tj(|| j"dd�}tj%|�| _)t*tj+|j,�| j�| _-| j)| j&| j.  | j-| j/  }tj0j1d| j&� tj0j1d| j)� tj0j1d| j-� tj0j1d|� tj2d�| _3tj4|| j3�}	| j5d k	�r�tj6|	| j5�\}	}
t7t8|	| j3��}	W d Q R X tjddd	��� tj0j1dtj%| j�� tj0j1dtj%| j �� tj0j1dtj%| j�� | j9�rptj0j:d| j� tj0j:d| j � tj0j:d| j� tj;| j��r`tj0j<d|j=� ntj0j:d|j=� W d Q R X tj>j?| j | j@| jAd�}|jB|	�| _C|| _D|| _E|jF| _F|jG| _G|jH| _H|jI| _ItjJ� jK| jd� tj0jL� | _0W d Q R X W d Q R X d S )N)�num_cpur2   �   �reuseFr>   T)rX   �custom_getter�loss)rX   �	action_ph)�namer7   r8   r4   Zinitial_obs_valuezself.sor_omega)�axis)�axes�entropy_loss�policy_gradient_loss�value_function_loss�model�
input_info�discounted_rewardsr-   �	advantage�observation)r-   �decayr+   )�session)Mr   r   �tf�Graphr2   �
as_default�set_random_seedr!   r   �make_sessionr"   r3   �n_envsr%   r5   r0   rH   �
issubclassr   r
   �observation_spacerO   r    �variable_scope�outer_scope_getter�pdtype�sample_placeholderr6   �placeholder�float32r7   r8   r4   rG   �	sor_omega�proba_distribution�neglogp�reduce_meanr;   �expand_dims�	tensordotr9   r   �squeeze�
value_flatr:   r(   r'   rE   �scalar�get_trainable_varsr<   �	gradientsr)   �clip_by_global_norm�list�zipr/   �	histogram�is_image�imagerS   �train�RMSPropOptimizerr*   r+   �apply_gradientsr=   r>   r?   r@   rA   rB   rC   �global_variables_initializer�run�	merge_all)rJ   �n_batch_step�n_batch_trainr?   r>   �	neglogpacZpolicyyZpg_ggi_lossrZ   �grads�_�trainerrL   rL   rM   rI   i   s~    
"
zA2C.setup_modelc             C   sn  || }d}xt t|��D ]}| jj� }qW | jj|| j|| j|| j|| j	|| j
|| j|i}|dk	r|||| jj< ||| jj< |
dk	�r>| jr�d|	 d dkr�tjtjjd�}tj� }| jj| j| j| j| j| jg|||d�\}}}}}|
j|d|	| j  � n,| jj| j| j| j| j| jg|�\}}}}}|
j||	| j � n&| jj| j| j| j| jg|�\}}}}|||fS )a�  
        applies a training step to the model

        :param obs: ([float]) The input observations
        :params avg_init_states: averaged value of Initial states
        :param states: ([float]) The states (used for recurrent policies)
        :param rewards: ([float]) The rewards from the environment
        :param masks: ([bool]) Whether or not the episode is over (used for recurrent policies)
        :param actions: ([float]) The actions taken
        :param values: ([float]) The logits values
        :param update: (int) the current step iteration
        :param writer: (TensorFlow Summary.writer) the writer for tensorboard
        :return: (float, float, float) policy loss, value loss, policy entropy
        NrW   �
   r   )�trace_level)�options�run_metadatazstep%d)�range�lenrD   rB   r>   rS   r6   r7   r8   r4   rG   rw   �	states_ph�dones_phr/   ri   �
RunOptions�
FULL_TRACE�RunMetadatar3   r�   rE   r9   r:   r;   r=   �add_run_metadatar5   �add_summary)rJ   �obsZavg_init_states�sorted_omega�states�rewards�masks�actions�values�update�writer�advs�cur_lrr�   �td_map�run_optionsr�   rE   �policy_loss�
value_loss�policy_entropyrL   rL   rM   �_train_step�   s.    
& zA2C._train_step�d   c             C   s�  | j |�}t| j���` t| j| j||���@}| j�  t| j|| j	d�| _
t| j| | j| j| j| jd�}tj| jf�| _tdd�}	tj� }
�x�td|| j d �D �]�}|j� \
}}}}}}}}}}|	j|� | j||||||||| j| j |�
\}}}tj� |
 }t|| j | �}|d k	�rXt| j|j| j| j| j f�|j| j| jf�|| j�| _|  j| j7  _|d k	�r�|t � t!� �dk�r�P | jdkr�|| dk�s�|dkr�t"||�}t#j$d|� t#j$d	| j� t#j$d
|� t#j$dt%|�� t#j$dt%|�� t#j$dt%|�� t&|	�dk�r\t&|	d �dk�r\t#j'dt(dd� |	D ��� t#j'dt(dd� |	D ��� t#j)�  q�W W d Q R X W d Q R X | S )N)�initial_value�n_values�schedule)r%   r&   rH   �ggi_constantr�   )�maxlenrW   Fr   �nupdates�total_timesteps�fpsr�   r�   r   �ep_reward_meanc             S   s   g | ]}|d  �qS )�rrL   )�.0�ep_inforL   rL   rM   �
<listcomp>"  s    zA2C.learn.<locals>.<listcomp>�ep_len_meanc             S   s   g | ]}|d  �qS )�lrL   )r�   r�   rL   rL   rM   r�   #  s    )*�_init_num_timestepsr   r   r   r2   r.   �_setup_learnr   r-   r,   rD   �	A2CRunnerr   r%   r&   rH   r1   �np�zerosrn   rF   r   �timer�   r5   r�   �extendr�   �num_timesteps�intr   �reshape�locals�globalsr   r   �record_tabular�floatr�   �logkvr   �dump_tabular)rJ   r�   �callback�log_interval�tb_log_name�reset_num_timesteps�
new_tb_logr�   �runner�ep_info_buf�t_startr�   r�   Z	avg_value�wr�   r�   r�   r�   r�   �ep_infos�true_rewardr�   r�   r�   �	n_secondsr�   �explained_varrL   rL   rM   �learn�   sP    
"



 
  z	A2C.learnc             C   sl   | j | j| j| j| j| j| j| j| j| j	| j
| j| j| j| j| j| j| jd�}| j� }| j||||d� d S )N)r&   r%   r'   r(   r)   r-   r*   r+   r,   r   r   rp   rO   rn   r"   r!   �_vectorize_actionr    )�datar<   �cloudpickle)r&   r%   r'   r(   r)   r-   r*   r+   r,   r   r   rp   rO   rn   r"   r!   r�   r    �get_parameters�_save_to_file)rJ   �	save_pathr�   r�   �params_to_saverL   rL   rM   �save(  s(    
zA2C.save)r   r   r   r   r   r   r   r   r   r   NTNFNN)N)Nr�   r   T)F)�__name__�
__module__�__qualname__�__doc__r$   rU   rI   r�   r�   r�   �__classcell__rL   rL   )rK   rM   r      s      0X
/ 
7r   c                   s&   e Zd Zd� fdd�	Zdd� Z�  ZS )r�   r   �G�z��?c                s,   t t| �j|||d� || _|| _|| _dS )aC  
        A runner to learn the policy of an environment for an a2c model

        :param env: (Gym environment) The environment to learn from
        :param model: (Model) The model to learn
        :param n_steps: (int) The number of steps to run for each environment
        :param gamma: (float) Discount factor
        )r   rb   r%   N)r#   r�   r$   r&   rH   r�   )rJ   r   rb   rH   r�   r%   r&   )rK   rL   rM   r$   D  s    	zA2CRunner.__init__c          
      sh  g g g g g f\}}}}}�j }g }x�t�j�D ]�}�jj�j�j �j�\}	}
}}|jtj	�j�� |j|	� |j|
� |j�j� |	}t
�jjtjj�r�tj|	�jjj�jjj�}�jj|�\}}}}x(|D ] }|jd�}|dk	r�|j|� q�W |�_ |�_|�_|j|� q.W �jj� }�jjtj|��j �j�}tj|dd�� � j� }� fdd�t�j�D �}tj�fdd�t�j�D ��}|| }|j�j� tj|�jjd�jd	d�j�j�}tj|tj d�jdd	�}tj|�jjjd�jdd	�}tj|tj d�jdd	�}tj|tj!d�jdd	�}|dd�dd�f }|dd�d	d�f }tj	|�}�jj�j�j �j�j"� }xpt#t$|||��D ]\\}\}}}|j"� }|j"� }|d dk�r�t%||g |dg �j&�j�dd� }|||< �qxW |jd|j'd
d� �� }|jd|j'd
d� �� }|jd|j'd
d� �� }|jd|j'd
d� �� }|jd|j'd
d� �� }|� ||||||||f
S )z�
        Run a learning step of the model

        :return: ([float], [float], [float], [bool], [float], [float])
                 observations, states, rewards, masks, actions, values
        �episodeNr   )r]   c                s&   g | ]}t j� j� |k�d  d  �qS )r   )r�   �where�argsort)r�   �i)�averaged_state_valuerL   rM   r�   |  s    z!A2CRunner.run.<locals>.<listcomp>c                s   g | ]}d � j |  �qS )rW   )r�   )r�   r�   )rJ   rL   rM   r�   }  s    )�dtyperW   �   �����r�   r�   r�   )r�   r�   )r�   r�   )r�   r�   )r�   r�   )r�   )(r�   r�   r%   rb   r@   r�   �dones�appendr�   �copyrN   r   rO   rP   rQ   �Box�clip�low�high�get�initial_statesrB   r}   �meanr�   rH   �array�asarrayr�   �swapaxesr�   �batch_ob_shaperv   �bool�tolist�	enumerater�   r   r&   �shape)rJ   �mb_obs�
mb_rewards�
mb_actions�	mb_values�mb_dones�	mb_statesr�   r�   r�   r�   r�   �clipped_actionsr�   r�   r�   �infos�info�maybe_ep_inforC   Zinitial_state_valueZsoretd_indexr�   �omegar�   �mb_masks�true_rewards�last_values�nrB   rL   )r�   rJ   rM   r�   R  sd    




"
 &zA2CRunner.run)r   r�   )r�   r�   r�   r$   r�   r�   rL   rL   )rK   rM   r�   C  s   r�   )r�   �collectionsr   rP   �numpyr�   �
tensorflowri   Zstable_baselinesr   Zstable_baselines.commonr   r   r   r   r   Z$stable_baselines.common.policies_ggir	   r
   �stable_baselines.common.runnersr   Zstable_baselines.a2c_ggi.utilsr   r   r   r   r   �stable_baselines.ppo2.ppo2r   r   r�   rL   rL   rL   rM   �<module>   s     5